# obnc-calypsi-65816

Provides a OCI Containerfile for installing the Calypsi 65816 compiler, and the obnc Oberon to C compiler.

## Getting started

This is tested using [Podman](https://www.podman.io)

### Build your container

With Podman installed:

```
cd ~/src/obnc-calypsi-65816
buildah build --layers -t obnc
```

This will get you a built container with the tools, with the name `obnc`.

### Working with the tools

An easy approach to working with these tools is to mount your local development folder
into a running container, and use the shell from within that container.

Example, this will mount your `$HOME/src` folder into a container, and give you a shell
inside that container:

```
podman run -it --volume $HOME/src:/root/src obnc
```

You can then see your project folders inside that container, for example:

```
cd ~/src/obnc-calypsi-65816
export CC=/usr/local/lib/calypsi-65816-5.4/bin/cc65816
obnc -V -x hello.obn
```

## Warnings

Calypsi is different from GCC in how it processes arguments - this `obnc` call above will fail, because `cc65816`
doesn't seem to like getting multiple filenames on the same compiler line, and it does not like specifying the
library names either, since linking is a separate task using that compiler.

As I come up with ways around that, I'll update this doc.

## License

Public Domain. This has no express licensing, it's meant entirely to be informative. Use it however you wish.

